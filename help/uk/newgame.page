<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" id="newgame" xml:lang="uk">
      
  <info>
    <title type="sort">1</title>
    <link type="guide" xref="index#play"/>
    <link type="seealso" xref="gametypes"/>
    <desc>Запуск програми і гра.</desc>
    <revision pkgversion="2.30" version="0.1" date="2010-01-10" status="draft"/>
    <revision pkgversion="2.30" version="0.2" date="2010-01-21" status="review"/>
   <credit type="author">
    <name>Milo Casagrande</name>
    <email>milo@ubuntu.com</email>
   </credit>
   <license>
     <p>Creative Commons Share Alike 3.0</p>
   </license>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Юрій Чорноіван</mal:name>
      <mal:email>yurchor@ukr.net</mal:email>
      <mal:years>2020</mal:years>
    </mal:credit>
  </info>

  <title>Сеанс гри</title>
  
  <section id="game-start">
    <title>Запуск нового сеансу</title>
    <p>Щоб розпочати груп, виконайте одну з таких дій:</p>
    <list>
      <item>
        <p>Виберіть пункт меню <guiseq><gui style="menu">Гра</gui><gui style="menuitem">Нова гра</gui></guiseq> і виберіть тип гри.</p>
      </item>
      <item>
        <p>Клацніть на одній з кнопок на панелі інструментів.</p>
      </item>
    </list>
    <p>Кнопки панелі інструментів призначено для виконання таких дій:</p>
    <table frame="none" rules="none" shade="none">
      <tr>
        <td>
        <terms>
          <item>
            <title>Усі</title>
            <p>Запускає нову гру із використанням усіх доступних типів гри.</p>
          </item>
        </terms>
        </td>
        <td>
          <media type="image" mime="image/png" src="figures/all-games.png" its:translate="no">
            <p>
              All games button
            </p>
          </media>
        </td>
      </tr>
      <tr>
        <td>
        <terms>
          <item>
            <title>Логіка</title>
            <p>Запускає нову гру із використанням лише логічних ігор.</p>
          </item>
        </terms>
        </td>
        <td>
          <media type="image" mime="image/png" src="figures/logic-games.png" its:translate="no">
            <p>
            Logic games button
            </p>
          </media>
        </td>
      </tr>
      <tr>
        <td>
        <terms>
          <item>
            <title>Обчислення</title>
            <p>Запускає нову гру, у якій будуть лише ігри на обчислення.</p>
          </item>
        </terms>
        </td>
        <td>
          <media type="image" mime="image/png" src="figures/math-games.png" its:translate="no">
            <p>
              Calculation games button
            </p>
          </media>
        </td>
      </tr>
      <tr>
        <td>
        <terms>
          <item>
            <title>Пам'ять</title>
            <p>Запускає нову гру, у якій будуть лише ігри на запам'ятовування.</p>
          </item>
        </terms>
        </td>
        <td>
          <media type="image" mime="image/png" src="figures/memory-games.png" its:translate="no">
            <p>
              Memory games button
            </p>
          </media>
        </td>
      </tr>
      <tr>
        <td>
        <terms>
          <item>
            <title>Слова</title>
            <p>Запускає нову гру, у якій будуть лише ігри у словесні аналогії.</p>
          </item>
        </terms>
        </td>
        <td>
          <media type="image" mime="image/png" src="figures/verbal-games.png" its:translate="no">
            <p>
              Verbal games button
            </p>
          </media>
        </td>
      </tr>
    </table>
    <note>
      <p>Ці описи також стосуються типів гри, які ви можете вибрати у меню <gui style="menu">Гра</gui>.</p>
    </note>
  </section>
  <section id="game-play">
    <title>Гра протягом сеансу</title>
    <note style="tip">
      <p>Під час гри завжди уважно читайте настанови!</p>
    </note>
    <p>Сеанс гри розпочинається з показу завдання. Після цього, програма попросить вас дати відповідь. У нижній частині вікна розташовано основний набір засобів керування, яким ви можете скористатися для взаємодії із грою.</p>
    <p>Щойно ви матимете відповідь, введіть її до поля для введення тексту <gui style="input">Відповідь</gui> і натисніть кнопку <gui style="button">Гаразд</gui>.</p>
    <p>Щоб перейти до наступної гри, натисніть кнопку <gui style="button">Наступний</gui>.</p>
    <note>
      <p>Якщо ви натиснете кнопку <gui style="button">Наступний</gui> до завершення раунду гри або не надавши ніякої відповіді, гру не буде враховано у ваших результатах.</p>
    </note>
  </section>
</page>
