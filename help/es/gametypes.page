<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" id="gametypes" xml:lang="es">
  <info>
    <title type="sort">3</title>
    <desc>Qué tipo de juegos puedes jugar.</desc>
    <link type="guide" xref="index#play"/>
    <link type="seealso" xref="gameplay"/>
    <revision pkgversion="2.30" version="0.1" date="2010-01-10" status="draft"/>
    <revision pkgversion="2.30" version="0.2" date="2010-01-21" status="review"/>    
    <license>
      <p>Creative Commons Compartir Igual 3.0</p>
    </license>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Daniel Mustieles</mal:name>
      <mal:email>daniel.mustieles@gmail.com</mal:email>
      <mal:years>2010-2018</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jorge González</mal:name>
      <mal:email>jorgegonz@svn.gnome.org</mal:email>
      <mal:years>2010</mal:years>
    </mal:credit>
  </info>
  <title>Tipos de juegos</title>

  <p><app>gbrainy</app> proporciona los siguientes tipos de juegos:</p>
  
  <table frame="none" rules="none" shade="none">
    <tr>
      <td>
      <terms>
        <item>
          <title>Puzles de lógica</title>
          <p>Juegos destinados a retar tu capacidad de razonamiento y pensamiento. Estos juegos están basados en secuencias de elementos, razonamiento espacial y visual o relaciones entre elementos.</p>
        </item>
      </terms>
      </td>
      <td>
        <media type="image" mime="image/png" src="figures/logic-games.png" its:translate="no">
          <p>
            Logic games logo
          </p>
        </media>
      </td>
    </tr>
    <tr>
      <td>
      <terms>
        <item>
          <title>Cálculos mentales</title>
          <p>Juegos basados en operaciones aritméticas diseñados para probar tu capacidad de cálculo mental. Juegos que requieren que el jugador use multiplicaciones, divisiones, sumas y restas combinadas de diferentes formas.</p>
        </item>
      </terms>
      </td>
      <td>
        <media type="image" mime="image/png" src="figures/math-games.png" its:translate="no">
          <p>
            Calculation games logo
          </p>
        </media>
      </td>
    </tr>
    <tr>
      <td>
      <terms>
        <item>
          <title>Entrenadores de memoria</title>
          <p>Juegos diseñados para retar tu memoria a corto plazo. Estos juegos muestran colecciones de objetos y preguntan al jugador acerca de ellos, algunas veces estableciendo relaciones entre figuras, palabras o colores.</p>
        </item>
      </terms>
      </td>
      <td>
        <media type="image" mime="image/png" src="figures/memory-games.png" its:translate="no">
          <p>
            Memory games logo
          </p>
        </media>
      </td>
    </tr>
    <tr>
      <td>
      <terms>
        <item>
          <title>Analogías verbales</title>
          <p>Juegos que retan tu aptitud verbal. Juegos que preguntan al jugador para que identifique causa y efecto, uso de sinónimos o antónimos y uso de su vocabulario.</p>
        </item>
      </terms>
      </td>
      <td>
        <media type="image" mime="image/png" src="figures/verbal-games.png" its:translate="no">
          <p>
            Verbal analogies games logo
          </p>
        </media>
      </td>
    </tr>
  </table>
</page>
