<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" id="pause" xml:lang="pt-BR">
      
  <info>
    <title type="sort">4</title>
    <link type="guide" xref="index#play"/>
    <link type="seealso" xref="newgame"/>
    <desc>Como pausar ou finalizar um jogo.</desc>
    <revision pkgversion="2.30" version="0.1" date="2010-01-10" status="draft"/>
    <credit type="author">
      <name>Milo Casagrande</name>
      <email>milo@ubuntu.com</email>
    </credit>
    <license>
      <p>Creative Commons Compartilhada Igual 3.0</p>
    </license>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Flamarion Jorge</mal:name>
      <mal:email>jorge.flamarion@gmail.com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Rafael Fontenelle</mal:name>
      <mal:email>rafaelff@gnome.org</mal:email>
      <mal:years>2013, 2016, 2018</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Enrico Nicoletto</mal:name>
      <mal:email>hiko@duck.com</mal:email>
      <mal:years>2022</mal:years>
    </mal:credit>
  </info>

  <title>Pausando/Finalizando um jogo</title>

  <section id="pause-pause">
    <title>Pausar e resumir um jogo</title>
    <p>Para pausar um jogo para que você possa continuá-lo a partir do mesmo ponto posteriormente, faça uma das opções a seguir:</p>
    <list>
      <item>
        <p>Escolha <guiseq><gui style="menu">Jogo</gui><gui style="menuitem">Pausar jogo</gui></guiseq>.</p>
      </item>
      <item>
        <p>Clique no botão de <gui style="button">Pausa</gui> na barra de ferramentas.</p>
      </item>
    </list>
    <p>Para continuar o jogo após ter pausado-o, faça uma das opções a seguir:</p>
    <list>
      <item>
        <p>Escolha <guiseq><gui style="menu">Jogo</gui><gui style="menuitem">Pausar jogo</gui></guiseq>.</p>
      </item>
      <item>
        <p>Clique no botão <gui style="button">Retomar</gui> na barra de ferramentas.</p>
      </item>
    </list>
  </section>
  
  <section id="pause-stop">
    <title>Finalizar um jogo</title>
    <p>Para finalizar um novo jogo, faça uma das opções a seguir:</p>
    <list>
      <item>
        <p>Escolha <guiseq><gui style="menu">Jogo</gui><gui style="menuitem">Finalizar jogo</gui></guiseq>.</p>
      </item>
      <item>
        <p>Clique no botão <gui style="button">Fim</gui> na barra de ferramentas.</p>
      </item>
    </list>
  </section>
</page>
