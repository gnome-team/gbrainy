<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" id="score" xml:lang="pt-BR">
      
  <info>
    <link type="guide" xref="index#play"/>
    <link type="seealso" xref="history"/>
    <desc>Como os pontos dos jogadores são calculados.</desc>
    <revision pkgversion="2.30" version="0.1" date="2010-01-22" status="draft"/>
    <credit type="author">
      <name>Milo Casagrande</name>
      <email>milo@ubuntu.com</email>
    </credit>
    <license>
      <p>Creative Commons Compartilhada Igual 3.0</p>
    </license>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Flamarion Jorge</mal:name>
      <mal:email>jorge.flamarion@gmail.com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Rafael Fontenelle</mal:name>
      <mal:email>rafaelff@gnome.org</mal:email>
      <mal:years>2013, 2016, 2018</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Enrico Nicoletto</mal:name>
      <mal:email>hiko@duck.com</mal:email>
      <mal:years>2022</mal:years>
    </mal:credit>
  </info>

  <title>Pontos e tempos de jogos</title>
  <p>Se você responder um quebra-cabeça incorretamente, você receberá nenhuma pontuação por ele.</p>
  <p>Se você responder um quebra-cabeça corretamente, você receberá uma pontuação que dependerá do tempo levado para resolver o problema e se você usou a dica durante o jogo.</p>
  <p>A tabela a seguir resume as diferentes durações de jogo (em segundos) baseado no nível de dificuldade.</p>
  <table frame="all" rules="all">
    <tr>
      <td><p/></td>
      <td><p>Fácil</p></td>
      <td><p>Médio</p></td>
      <td><p>Mestre</p></td>
    </tr>
    <tr>
      <td><p>Quebra-cabeças lógicos</p></td>
      <td><p>156</p></td>
      <td><p>120</p></td>
      <td><p>110</p></td>
    </tr>
    <tr>
      <td><p>Cálculo mental</p></td>
      <td><p>78</p></td>
      <td><p>60</p></td>
      <td><p>55</p></td>
    </tr>
    <tr>
      <td><p>Treino de memória</p></td>
      <td><p>39</p></td>
      <td><p>30</p></td>
      <td><p>27</p></td>
    </tr>
    <tr>
      <td><p>Analogias verbais</p></td>
      <td><p>39</p></td>
      <td><p>30</p></td>
      <td><p>27</p></td>
    </tr>
  </table>
  <p>Com o tempo esperado para o nível de dificuldade e o tempo que você levou para completar o jogo, a seguinte lógica é aplicada:</p>
  <list>
    <item>
      <p>Se você levar menos do que o tempo esperado para completar o jogo, você receberá 10 pontos.</p>
    </item>
    <item>
      <p>Se você levar mais do que o tempo esperado para completar o jogo, você receberá 8 pontos.</p>
    </item>
    <item>
      <p>Se você levar mais de 2x o tempo esperado para completar o jogo, você receberá 7 pontos.</p>
    </item>
    <item>
      <p>Se você levar mais de 3x o tempo esperado para completar o jogo, você receberá 6 pontos.</p>
    </item>
    <item>
      <p>Se você usar uma dica, você pontuará apenas 80% da pontuação original.</p>
    </item>
  </list>
  
  <section id="computing-scores">
    <title>Computando os totais</title>
    <p><app>gbrainy</app> mantém um acompanhamento dos diferentes tipos de jogos que você jogou. Para computar a pontuação final de cada conjunto de tipos de jogos ele suma todos os resultados do mesmo tipo de jogo e, então, aplica um fator baseado: no logaritmo de 10 para o nível fácil; no logaritmo de 20 para o nível médio; e no logaritmo de 30 para o nível mestre.</p>
	  <p>Isso significa que quando se está jogando no nível de dificuldade média, para se obter uma pontuação de 100 pontos você precisa obter 10 pontos em pelo menos 20 jogos de todo tipo de jogo jogado.</p>
	  <p>Isso pode soar desafiador, mas isso permite aos jogadores compararem pontuações de jogos de sessões diferentes (no histórico de jogos do jogador) e permite melhor acompanhamento do progresso do jogador por todos os jogos jogados.</p>
  </section>
</page>
