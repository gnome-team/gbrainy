<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" id="history" xml:lang="pt-BR">
  <info>
    <link type="guide" xref="index#play"/>
    <desc>Acesse seu histórico pessoal de jogos.</desc>
    <revision pkgversion="2.30" version="0.1" date="2010-01-22" status="draft"/>
    <credit type="author">
      <name>Milo Casagrande</name>
      <email>milo@ubuntu.com</email>
    </credit>
    <license>
      <p>Creative Commons Compartilhada Igual 3.0</p>
    </license>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Flamarion Jorge</mal:name>
      <mal:email>jorge.flamarion@gmail.com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Rafael Fontenelle</mal:name>
      <mal:email>rafaelff@gnome.org</mal:email>
      <mal:years>2013, 2016, 2018</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Enrico Nicoletto</mal:name>
      <mal:email>hiko@duck.com</mal:email>
      <mal:years>2022</mal:years>
    </mal:credit>
  </info>

  <title>Histórico pessoal de jogos</title>
  
  <p>O diálogo <gui>Histórico de partidas dos jogadores</gui> mostra sua performance em cada tipo de jogo durante as últimas sessões de jogo.</p>
  
  <section id="history-personal">
    <title>Visualizar o histórico de jogos</title>
    <p>Para acessar seu histórico pessoal de jogos, escolha <guiseq><gui style="menu">Ver</gui><gui style="menuitem">Histórico de partidas dos jogadores</gui></guiseq>.</p>
  </section>
  
  <section id="history-change-graphic">
    <title>Selecionar quais resultados para mostrar</title>
    <p>Para selecionar os resultados que serão mostrados no gráfico, use as diferentes caixas de seleção localizadas sob o gráfico.</p>
  </section>
  
  <section id="history-save-prefs">
    <title>Mudar o número de jogos salvos</title>
    <p><app>gbrainy</app> salva a pontuação do jogador de forma que seja possível mostrar como ela se desenvolve.</p>
    <p>Para alterar o número de sessões gravadas ou como vários jogos serão armazenados no histórico, faça o seguinte:</p>
    <steps>
      <item>
        <p>Escolha <guiseq><gui style="menu">Configurações</gui><gui style="menuitem"> Preferências</gui></guiseq>.</p>
      </item>
      <item>
        <p>Na seção <gui>Histórico de partidas dos jogadores</gui>:</p>
        <list>
          <item>
            <p>Use a primeira caixa de incremento para alterar quantas sessões de jogo precisam ser registradas antes que você possa começar a ver os resultados no gráfico de histórico.</p>
          </item>
          <item>
            <p>Use a segunda caixa de incremento para alterar quantas sessões de jogos serão mostradas no gráfico de histórico.</p>
          </item>
        </list>
      </item>
    </steps>
  </section>
</page>
