<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" id="pause" xml:lang="de">
      
  <info>
    <title type="sort">4</title>
    <link type="guide" xref="index#play"/>
    <link type="seealso" xref="newgame"/>
    <desc>Wie man ein Spiel anhält oder beendet.</desc>
    <revision pkgversion="2.30" version="0.1" date="2010-01-10" status="draft"/>
    <credit type="author">
      <name>Milo Casagrande</name>
      <email>milo@ubuntu.com</email>
    </credit>
    <license>
      <p>Creative Commons Share Alike 3.0</p>
    </license>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Wolfgang Stöggl</mal:name>
      <mal:email>c72578@yahoo.de</mal:email>
      <mal:years>2010</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Paul Seyfert</mal:name>
      <mal:email>pseyfert@mathphys.fsk.uni-heidelberg.de</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Mario Blättermann</mal:name>
      <mal:email>mario.blaettermann@gmail.com</mal:email>
      <mal:years>2017, 2018</mal:years>
    </mal:credit>
  </info>

  <title>Anhalten/Beenden eines Spiels</title>

  <section id="pause-pause">
    <title>Anhalten und Fortsetzen eines Spiels</title>
    <p>Um ein Spiel anzuhalten, so dass Sie es zu einem späteren Zeitpunkt wieder fortsetzen können, führen Sie einen der folgenden Schritte durch:</p>
    <list>
      <item>
        <p>Wählen Sie <guiseq><gui style="menu">Spiel</gui><gui style="menuitem">Spiel pausieren / fortsetzen</gui></guiseq>.</p>
      </item>
      <item>
        <p>Klicken Sie auf den <gui style="button">Pause</gui>-Knopf in der Werkzeugleiste.</p>
      </item>
    </list>
    <p>Um ein Spiel fortzusetzen, nachdem es pausiert wurde, führen Sie einen der folgenden Schritte durch:</p>
    <list>
      <item>
        <p>Wählen Sie <guiseq><gui style="menu">Spiel</gui><gui style="menuitem">Spiel pausieren / fortsetzen</gui></guiseq>.</p>
      </item>
      <item>
        <p>Klicken Sie auf den <gui style="button">Fortsetzen</gui>-Knopf in der Werkzeugleiste.</p>
      </item>
    </list>
  </section>
  
  <section id="pause-stop">
    <title>Ein Spiel beenden</title>
    <p>Um ein Spiel zu beenden, führen Sie folgende Schritte durch:</p>
    <list>
      <item>
        <p>Wählen Sie <guiseq><gui style="menu">Spiel</gui><gui style="menuitem">Spiel beenden</gui></guiseq>.</p>
      </item>
      <item>
        <p>Klicken Sie auf den <gui style="button">Beenden</gui>-Knopf in der Werkzeugleiste.</p>
      </item>
    </list>
  </section>
</page>
